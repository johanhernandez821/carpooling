package com.carpooling.service.implementation;

import com.carpooling.dto.MessageDto;
import com.carpooling.dto.PersonDto;
import com.carpooling.exception.PersonNotFoundException;
import com.carpooling.mapper.PersonMapper;
import com.carpooling.model.PersonEntity;
import com.carpooling.repository.PersonRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class PersonServiceImplTest {

    @Mock
    PersonRepository personRepository;

    @Spy
    PersonMapper personMapper;

    @InjectMocks
    PersonServiceImpl personService;

    @Test
    void listAll() {
        List<PersonEntity> listPersons = List.of(this.getPerson());
        when (personRepository.findAll()).thenReturn(listPersons);
        List <PersonDto> personDtoList = this.personService.listAll();
        assertNotNull(personDtoList);
    }

    @Test
    void create() {
        MessageDto messageDto = MessageDto.builder().message("Save User!!").build();
        when(personRepository.save(any())).thenReturn(this.getPerson());
        assertEquals(messageDto.getMessage(), personService.create(this.getPersonDto()).getMessage());
    }

    @Test
    void update() {
        MessageDto messageDto = MessageDto.builder().message("Update User!!").build();
        when(personRepository.findById(anyLong())).thenReturn(Optional.of(this.getPerson()));
        when(personRepository.save(any())).thenReturn(this.getPerson());
        assertEquals(messageDto.getMessage(), personService.update(this.getPersonDto(), 3L).getMessage());

        when(personRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(PersonNotFoundException.class,
                () -> personService.update(this.getPersonDto(), 3L));
    }

    @Test
    void delete() {
        MessageDto messageDto = MessageDto.builder().message("Delete User!!").build();
        assertEquals(messageDto.getMessage(), personService.delete(1L).getMessage());
    }

    @Test
    void findById() {
    }

    public PersonEntity getPerson(){

        PersonEntity person1 = new PersonEntity();
        person1.setId(1L);
        person1.setAddress("Av 5");
        person1.setEmail("jasdad@gma.com");
        person1.setName("Pepo");
        person1.setLastName("Perez");
        person1.setPassword("Aaaa1234@");
        person1.setPhoneNumber("321698789");

        return person1;
    }

    public PersonDto getPersonDto(){

        PersonDto person1 = new PersonDto();
        person1.setAddress("Av 5");
        person1.setEmail("jasdad@gma.com");
        person1.setName("Pepo");
        person1.setLastName("Perez");
        person1.setPassword("Aaaa1234@");
        person1.setPhoneNumber("321698789");

        return person1;
    }

}
package com.carpooling.service.implementation;



import com.carpooling.dto.MessageDto;
import com.carpooling.dto.PersonDto;
import com.carpooling.exception.PersonNotFoundException;
import com.carpooling.mapper.PersonMapper;
import com.carpooling.model.PersonEntity;
import com.carpooling.repository.PersonRepository;
import com.carpooling.service.PersonService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {

    PersonRepository personRepository;

    PersonMapper personMapper;

    public PersonServiceImpl(PersonRepository personRepository, PersonMapper personMapper) {
        this.personRepository = personRepository;
        this.personMapper = personMapper;
    }

    @Override
    public List<PersonDto> listAll() {
        return personMapper.toListPersonDto(personRepository.findAll());
    }

    @Override
    public MessageDto create(PersonDto person) {
        personRepository.save(personMapper.toPersonEntity(person));
        return MessageDto.builder().message("Save User!!").build();
    }

    @Override
    public MessageDto update(PersonDto person, Long id) {
        Optional<PersonEntity> personEntity = personRepository.findById(id);

        if (personEntity.isEmpty()) {
            throw new PersonNotFoundException("Person Not Found¡");
        }

        PersonEntity oldPerson = PersonEntity.builder()
                .id(id).name(person.getName())
                .lastName(person.getLastName())
                .phoneNumber(person.getPhoneNumber())
                .address(person.getAddress())
                .email(person.getEmail())
                .password(person.getPassword()).build();

        personRepository.save(oldPerson);
        return MessageDto.builder().message("Update User!!").build();
    }

    @Override
    public MessageDto delete(Long id) {
        personRepository.deleteById(id);
        return MessageDto.builder().message("Delete User!!").build();
    }
}

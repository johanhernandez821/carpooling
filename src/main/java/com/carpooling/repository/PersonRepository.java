package com.carpooling.repository;

import com.carpooling.model.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository <PersonEntity, Long> {
}

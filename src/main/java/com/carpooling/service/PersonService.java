package com.carpooling.service;

import com.carpooling.dto.MessageDto;
import com.carpooling.dto.PersonDto;
import com.carpooling.model.PersonEntity;

import java.util.List;

public interface PersonService {
    List<PersonDto> listAll();

    MessageDto create (PersonDto person);

    MessageDto update (PersonDto person, Long id);

    MessageDto delete (Long id);

}

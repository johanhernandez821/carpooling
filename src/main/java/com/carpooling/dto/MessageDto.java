package com.carpooling.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder(toBuilder = true)
public class MessageDto {
    public String message;

    public MessageDto(String message) {
        this.message = message;
    }
}

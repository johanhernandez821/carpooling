package com.carpooling.mapper;

import com.carpooling.dto.PersonDto;
import com.carpooling.model.PersonEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface PersonMapper {
    PersonEntity toPersonEntity (PersonDto personDto);

    PersonDto toPersonDto (PersonEntity personEntity);

    List<PersonDto> toListPersonDto (List <PersonEntity> personEntityList);
}

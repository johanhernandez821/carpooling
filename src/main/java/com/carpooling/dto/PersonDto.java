package com.carpooling.dto;


import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Getter
@Setter
@Valid
public class PersonDto implements Serializable {

    @NotEmpty(message = "Name empty")
    private String name;

    @NotEmpty(message = "lastName empty")
    private String lastName;

    @NotEmpty(message = "Phone Number empty")
    private String phoneNumber;

    @NotEmpty(message = "Address empty")
    private String address;

    @Email(message = "bad email")
    @NotEmpty(message = "email empty")
    private String email;

    @Pattern(regexp =  "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])([A-Za-z\\d$@$!%*?&]|[^ ]){8,15}$")
    @NotEmpty(message = "password empty")
    private String password;

    public PersonDto () {}
}
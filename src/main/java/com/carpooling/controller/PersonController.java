package com.carpooling.controller;

import com.carpooling.dto.MessageDto;
import com.carpooling.dto.PersonDto;
import com.carpooling.service.PersonService;
import com.carpooling.service.implementation.PersonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {

    PersonServiceImpl personService;

    public PersonController(PersonServiceImpl personService) {
        this.personService = personService;
    }

    @PostMapping
    public ResponseEntity<MessageDto> create (@Valid @RequestBody PersonDto personDto) {
        return new ResponseEntity<>(personService.create(personDto), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<PersonDto>> listAll (){
        return new ResponseEntity<>(personService.listAll(), HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<MessageDto> update (@PathVariable Long id, @RequestBody PersonDto personDto) {
     return new ResponseEntity<>( personService.update(personDto, id), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<MessageDto> deletePerson (@PathVariable Long id){
        return new ResponseEntity<>(personService.delete(id), HttpStatus.OK);
    }

}
